#include <iostream>
#include "greenscreenator.h"

void greenscreenator::greenscreenate(
		const char *in_name,
		const char *out_name,
		int R, int G, int B){

	cv::namedWindow("Input",CV_WINDOW_AUTOSIZE); 
	cv::namedWindow("First",CV_WINDOW_AUTOSIZE); 
	cv::namedWindow("Difference",CV_WINDOW_AUTOSIZE);
	cv::namedWindow("Output",CV_WINDOW_AUTOSIZE); 
	
	capture.open(in_name);
	
	cv::Mat frame, first_frame, first_frame_blurred,frame_blurred, diff_frame;
	
	for(int k=0;k<10;++k)
		capture.read(frame);
	diff_frame = frame.clone();
	first_frame = frame.clone();
	
//	GaussianBlur( src, dst, Size( i, i ), 0, 0 )
	
	while(1){
     
		bool bSuccess = capture.read(frame); // read a new frame from video
		if (!bSuccess) {
	  	std::cout << "Cannot read the frame from video file" << std::endl;
			break;
		}
		cv::absdiff(frame,first_frame,diff_frame);
		cv::imshow("Difference", diff_frame); //show the frame in "MyVideo" window
		cv::imshow("Input", frame); //show the frame in "MyVideo" window
		cv::imshow("first", first_frame); //show the frame in "MyVideo" window
		if(cv::waitKey(30) == 27) {
			 std::cout << "esc key is pressed by user" << std::endl;
			break; 
		}
  }
	
}

