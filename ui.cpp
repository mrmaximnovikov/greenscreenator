/**
*   @file   ui.cpp
*   @author Maxim Novikov
*   @brief  GreenSreenator main window grnrration
*   @date   Jan 2014
*/ 

#include "ui.h"


greenscreenator greenscreenatorUI::videoprocessor;
	

std::string greenscreenatorUI::in_filename;
std::string greenscreenatorUI::out_filename;

void greenscreenatorUI::ui_init(int *argc, char ***argv){
	
	gtk_init(argc, argv);	// gtk initialisation
	
	// GUI controls	initialisation
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL),
	browse_input_button = gtk_button_new_with_label("Browse input video"),
	browse_output_button = gtk_button_new_with_label("Browse output video"),
	run_button = gtk_button_new_with_label("Run Greenscreenator!"),
	vertical_box =  gtk_vbox_new(FALSE,0);
	
	// connect the closing of window to closing the application 
	g_signal_connect(G_OBJECT(window),"destroy",G_CALLBACK(gtk_main_quit),0);
	
	//pack controls
	gtk_box_pack_start (GTK_BOX(vertical_box), browse_input_button, FALSE,FALSE,0);
	gtk_box_pack_start (GTK_BOX(vertical_box), browse_output_button, FALSE,FALSE,0);
	gtk_box_pack_start (GTK_BOX(vertical_box), run_button, FALSE,FALSE,0);
	
  gtk_container_add(GTK_CONTAINER(window), vertical_box);
     
	//show window
	gtk_widget_show (window);
	gtk_widget_show (browse_input_button);
	gtk_widget_show (browse_output_button);
	gtk_widget_show (run_button);
	gtk_widget_show (vertical_box);
	
	
	//connect to slots
	g_signal_connect(GTK_BUTTON(browse_input_button), "clicked", G_CALLBACK(greenscreenatorUI::get_input_file), NULL);
	g_signal_connect(GTK_BUTTON(browse_output_button), "clicked", G_CALLBACK(greenscreenatorUI::get_output_file), NULL);
	
	g_signal_connect(GTK_BUTTON(run_button), "clicked", G_CALLBACK(greenscreenatorUI::run_grenscreenator), NULL);
	
}

void greenscreenatorUI::main(void){
	gtk_main();
}


void greenscreenatorUI::get_input_file(GtkButton *button, gpointer data){
	GtkWidget *dialog;
	dialog = gtk_file_chooser_dialog_new ("Open File",
     				      0,
     				      GTK_FILE_CHOOSER_ACTION_OPEN,
     				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
     				      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
     				      NULL);
     
  if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT){
     char *filename;
     filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
     greenscreenatorUI::in_filename = std::string(filename);
    	
     g_free (filename);
  }
     
     gtk_widget_destroy (dialog);
}

void greenscreenatorUI::get_output_file(GtkButton *button, gpointer data){
	GtkWidget *dialog;
     
     dialog = gtk_file_chooser_dialog_new ("Save File",
     				      0,
     				      GTK_FILE_CHOOSER_ACTION_SAVE,
     				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
     				      GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
     				      NULL);
     gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog), TRUE);
          
     if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
     {
       char *filename;
     
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
      	greenscreenatorUI::out_filename = std::string(filename);
    
        g_free (filename);
     }
     
     gtk_widget_destroy (dialog);	
}

void greenscreenatorUI::run_grenscreenator(GtkButton *button, gpointer data){
		videoprocessor.greenscreenate(
			greenscreenatorUI::in_filename.c_str(),
			greenscreenatorUI::out_filename.c_str());
}
	
