/**
*   @file   main.cpp
*   @author Maxim Novikov
*   @brief  GreenSreenator main 
*   @date   Jan 2014
*/ 


#include <cstdlib>
#include <gtk/gtk.h> 

#include "ui.h"

/**
* @fucnion main
* @brief   entry point for the program
* @param   argc  number or arguments
* @param   argv  array of string arguments
*/
int main( int argc, char **argv){

	greenscreenatorUI ui;
	ui.ui_init(&argc, &argv); // initialise user interface
	
	ui.main(); // pass control to ui
	
	return EXIT_SUCCESS;
}


