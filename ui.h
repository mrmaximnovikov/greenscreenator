#ifndef GREENSCREENATOR_UI_H
#define GREENSCREENATOR_UI_H


#include <string>
#include <gtk/gtk.h> 

#include "greenscreenator.h"

class greenscreenatorUI{
	
	GtkWidget 
			*window,
			*browse_input_button,
			*browse_output_button,
			*run_button,
			*vertical_box;
	
	static greenscreenator videoprocessor;
	
public:
	static std::string in_filename,out_filename;
	void ui_init(int *argc, char ***argv);
	void main(void);
	static void get_input_file(GtkButton *button, gpointer data);
	static void get_output_file(GtkButton *button, gpointer data);
	static void run_grenscreenator(GtkButton *button, gpointer data);
};

#endif //GREENSCREENATOR_UI_H

