#ifndef GREENSCREENATOR_H
#define GREENSCREENATOR_H

#include <opencv2/highgui/highgui.hpp>

class greenscreenator{
	cv::VideoCapture capture;
public:
	void greenscreenate(
		const char *in_name,
		const char *out_name,
		int R=0, int G=255, int B=0);
};

#endif // GREENSCREENATOR_H


