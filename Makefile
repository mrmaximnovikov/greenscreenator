all: ui.o greenscreenator_class.o
	g++ -Wall -Ofast -g -o build/greenscreenator ui.o greenscreenator_class.o   main.cpp `pkg-config --cflags --libs gtk+-2.0` `pkg-config opencv --libs --cflags`

ui.o:
	mkdir build; g++ -c -Wall -Ofast  -g -o ui.o ui.cpp  `pkg-config --cflags --libs gtk+-2.0` `pkg-config opencv --libs --cflags`

greenscreenator_class.o:
	mkdir build; g++ -c -Wall -Ofast  -g -o greenscreenator_class.o greenscreenator.cpp  `pkg-config --cflags --libs gtk+-2.0` `pkg-config opencv --libs --cflags`

clean:
	rm -rf build
	rm *.o
